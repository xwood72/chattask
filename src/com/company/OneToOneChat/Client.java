package com.company.OneToOneChat;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by xw on 15.06.17.
 */
public class Client extends Thread {
    private static String ip;
    private static int port;

    private static volatile DataInputStream in;
    private static volatile DataInputStream keyboard;
    private static volatile DataOutputStream out;

    public Client(String ip, int port) {

    }

    public static void main(String[] args) throws IOException {

//        Socket socket = new Socket("127.0.0.1", 7777);
        Socket socket = new Socket("10.240.17.57", 7777);

        //Scanner inScanner = new Scanner(System.in);
//        while (inScanner.hasNext()) {
//            dataOutputStream.writeUTF(inScanner.next());
//            inScanner.next();
//        }
        //dataOutputStream.writeUTF("Ololo!");
        //dataOutputStream.flush();

        InputStream sin = socket.getInputStream();
         in = new DataInputStream(sin);

        OutputStream sout = socket.getOutputStream();
         out = new DataOutputStream(sout);

        //Scanner keyboard = new Scanner(System.in);
        keyboard = new DataInputStream(System.in);

        Thread readMessage = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        if (in.readUTF() != null){
                            System.out.println(in.readUTF());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        Thread sendMessage = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    String mes;
                    try {
                        String msg;
//                        if (keyboard.hasNext()){
                        if (!(msg = keyboard.readUTF()).equals("")){
//                            out.writeUTF(keyboard.next());
                            out.writeUTF(msg);
                            out.flush();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        readMessage.start();
        sendMessage.start();

    }
}
