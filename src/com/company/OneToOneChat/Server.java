package com.company.OneToOneChat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

/**
 * Created by xw on 15.06.17.
 */


public class Server {
    private String ip;
    private int port;

    private static volatile DataInputStream in;
    private static volatile DataInputStream keyboard;
    private static volatile DataOutputStream out;


    public Server(String ip, int port) {
        this.ip = ip;
        this.port = port;
    }

    public static void main(String[] args) throws IOException {
//        ServerSocket serverSocket = new ServerSocket(7777);
//        Socket socket = serverSocket.accept(); //here the app stop and wait data to socket
//        InputStream istream = socket.getInputStream();
//        DataInputStream dis = new DataInputStream(istream);
//        System.out.println(dis.readUTF());

//        Socket socket = new Socket("127.0.0.1", 7777);
//        OutputStream outputStream = socket.getOutputStream();
//        DataOutputStream dataOutputStream = new DataOutputStream(outputStream);
//        dataOutputStream.writeUTF("Ololo!");
//        dataOutputStream.flush();
//        socket.close();

        ServerSocket sSocket = new ServerSocket(7777);
        Socket socket = sSocket.accept();

        InputStream sin = socket.getInputStream();
         in = new DataInputStream(sin);

        OutputStream sout = socket.getOutputStream();
         out = new DataOutputStream(sout);

        //Scanner keyboard = new Scanner(System.in);
         keyboard = new DataInputStream(System.in);

        Thread readMessage = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        if (in.readUTF() != null){
                            System.out.println(in.readUTF());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
//
        Thread sendMessage = new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    String mes;
                    try {
                        String msg;
//                        if (keyboard.hasNext()){
                        if (!(msg = keyboard.readUTF()).equals("")){
//                            out.writeUTF(keyboard.next());
                            out.writeUTF(msg);
                            out.flush();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        readMessage.start();
        sendMessage.start();
    }


}
